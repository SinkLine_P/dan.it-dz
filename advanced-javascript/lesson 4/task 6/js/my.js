const employee = {
  name: 'Vitalii',
  surname: 'Klichko',
}

const newEmployee = {
  age: 21,
  salary: 1000,
}

let newObject = {...employee, ...newEmployee}

console.log(newObject);