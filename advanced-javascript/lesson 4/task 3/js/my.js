const user1 = {
    name: "John",
    years: 30
};

let keyName = "name";
let keyAge = "years";
let keyAdmin = "isAdmin";

if (user1.isAdmin == undefined) {
    user1.isAdmin = false;
}

let { [keyName]: name, [keyAge]: age, [keyAdmin]: isAdmin } = user1;

console.log(name);
console.log(age);
console.log(isAdmin);
