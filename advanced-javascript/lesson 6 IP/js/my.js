'use strict';

const ipBtn = document.getElementById("btn-ip");


ipBtn.addEventListener("click", () => {
	(async () => {
	let url = 'https://api.ipify.org/?format=json';
	let response = await fetch(url);
	let commits = await response.json();
	
	let urlScanner = 'http://ip-api.com/';
	let responseScanner = await fetch(urlScanner + "/json/" + commits.ip)
	let commitsScanner = await responseScanner.json();

	const country = document.createElement("p");
	const region = document.createElement("p");
	const city = document.createElement("p");
	const timezone = document.createElement("p");

	const root = document.getElementById("root");

    country.textContent = "Страна: " + commitsScanner.country;
    region.textContent = "Регион: " + commitsScanner.regionName;
    city.textContent = "Город: " + commitsScanner.city;
    timezone.textContent = "Часовой пояс: " + commitsScanner.timezone;

    root.append(country, region, city, timezone);
})()  
})

		