class Employee {
    constructor(name, surname, age, salary) {
        this.name = { first: name, last: surname },
            this.age = age,
            this.salary = salary
    }

    get fullName() {
        return this.name;
    }

    get myAge() {
        return this.age;
    }

    get mySalary() {
        return this.salary;
    }


    
}

class Programmer extends Employee {
    constructor(salary, lang) {
        super(salary);
        this.salary = salary;
        this.lang = lang;
    }

    get mySalary() {
        return this.salary * 3;
    }
}

class Programmer2 extends Employee {
    constructor(salary, lang) {
        super(salary);
        this.salary = salary;
        this.lang = lang;
    }

    get mySalary() {
        return this.salary * 2;
    }
}

class Programmer3 extends Employee {
    constructor(salary, lang) {
        super(salary);
        this.salary = salary;
        this.lang = lang;
    }

    get mySalary() {
        return this.salary * 15;
    }
}


const user = new Employee("Artem", "Popov", 17, 1000);

const programmer = new Programmer(user.salary, "ru");
const programmer2 = new Programmer2(user.salary, "en");
const programmer3 = new Programmer3(user.salary, "ua");

console.log(programmer.mySalary);
console.log(programmer2.mySalary);
console.log(programmer3.mySalary);





