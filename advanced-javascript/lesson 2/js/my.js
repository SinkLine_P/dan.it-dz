// "use strict";

const root = document.getElementById("root");
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

let parseComa;


Array.prototype.showNames = function () {
  const author = this.map(item => item.author === undefined);
  const name = this.map(item => item.name === undefined);
  const price = this.map(item => item.price === undefined);

  let authorError, nameError, priceError;
  const itemList = this.map(item => `<li>` + item.author + `</li>` + `<li>` + item.name + `</li>` + `<li>` + item.price + `</li>`);
  parseComa = JSON.parse(JSON.stringify(itemList).split('","').join(''));

  try {
    for (let i = 0; i < author.length; i++) {
      if (author[i] == true) {
        authorError = "Author";
      };
    };

    for (let i = 0; i < name.length; i++) {
      if (name[i] == true) {
        nameError = "Name";
      };
    };
    for (let i = 0; i < price.length; i++) {
      if (price[i] == true) {
        priceError = "Price";
      };
    };
    if (authorError == undefined) {
      authorError = "";
    };
    if (nameError == undefined) {
      nameError = "";
    };
    if (priceError == undefined) {
      priceError = "";
    };

    throw new Error("no properties " + `'${authorError}'` + " " + `'${nameError}'` + " " + `'${priceError}'`);
  } catch (e) {
    console.log(e.message);
  } finally {
    return parseComa;
  };
};

Array.prototype.delUndefined = () => {
  const parseUndefined = JSON.parse(JSON.stringify(parseComa).split('<li>undefined</li>').join(''));
  return parseUndefined;
};

const items = books.showNames();
const delUndef = items.delUndefined();

for (let i = 0; i < books.length; i++) {
  root.innerHTML = `<ul>` + delUndef + `</ul>`;
};

