const BASE_URL = "https://ajax.test-danit.com/api/swapi/films";

fetch(BASE_URL)
    .then(response => response.json())
    .then(commits => {
        commits.forEach(el => {
            const title = document.createElement("p");
            const episode = document.createElement("p");
            const content = document.createElement("p");
            const persons = document.createElement("p");
            const dates = document.createElement("p")
            const hr = document.createElement("hr")
            title.textContent = "Title: " + el.name;
            episode.textContent = "Episode: " + el.episodeId;
            content.textContent = "Executive summary: " + el.openingCrawl;
            document.getElementById("root").append(title, persons, dates, episode, content, hr);
            
            el.characters.forEach(url => {
                fetch(url)
                .then(response => response.json())
                .then(commits => {
                    persons.textContent = "Characters: Name - " + commits.name;
                    dates.textContent = "Characters: Date - " + commits.birthYear;
                    console.log(commits);
                })
            });

        })
    });

