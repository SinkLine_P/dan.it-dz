

$(document).on("click", "nav a", function(e) {
    e.preventDefault();
    let id  = $(this).attr('href');
    let top = $(id).offset().top;
    $('body, html').animate({
        scrollTop: top
    }, 1000);
});


$(document).scroll(function() {
    if ($(document).scrollTop() > 400) {
        $(".btn-fixed-up").addClass('show');
    } else {
        $(".btn-fixed-up").removeClass('show');
    }
});

$(document).on("click", ".btn-fixed-up", function(e) {
    e.preventDefault();
    $('body, html').animate({scrollTop: 0}, 1000);
});


$("#close-hot-news").click(function() {
    $("#section-close-hot-news").slideToggle("slow" , function () {
        
    });
});



