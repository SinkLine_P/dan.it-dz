function createNewUser() {
  let Fname = prompt("First Name: ");
  let Lname = prompt("Last Name: ");
  let birthday = prompt("Enter date 'dd.mm.yyyy': ");

  function getAge(birthday) {
    return Math.floor(((new Date - new Date(birthday.split(".").reverse().join('.'))) / 1000 / (60 * 60 * 24)) / 365.25)
  }

  function getPassword(Fname, Lname, birthday) {
    let pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
    let date = new Date(birthday.replace(pattern, '$3-$2-$1'));
    return Fname.substr(0, 1).toUpperCase() + Lname.toLowerCase() + date.getFullYear()
  }

  console.log(getAge(birthday))
  console.log(getPassword(Fname, Lname, birthday))
}
createNewUser()
