//PASSWORD
let i1Elements = document.getElementById("i1");
let i2Elements = document.getElementById("i2");
//PASSWORD-CONFIRM
let i1ElementsConfirm = document.getElementById("i1c");
let i2ElementsConfirm = document.getElementById("i2c");
//PASSWORD-VALIDATE
let firstInput = document.password;
let lastInput = document.password;
let err = document.getElementById("error");
let errText = "Нужно ввести одинаковые значения";
//FORM
let form = document.getElementById("form");
//MODAL
let modalText = document.getElementById("modal");


function show_hide_password(target) {
	let input = document.getElementById('password-input');

	if (input.getAttribute('type') == 'password') {
		target.classList.add('view');
		input.setAttribute('type', 'text');
		i1Elements.style.display = "block"
		i2Elements.style.display = "none"
	} else {
		target.classList.remove('view');
		input.setAttribute('type', 'password');
		i1Elements.style.display = "block"
		i2Elements.style.display = "block"
	}
	return false;
}

function show_hide_password_confirm(target) {
	let input = document.getElementById('password-confirm');



	if (input.getAttribute('type') == 'password') {
		target.classList.add('view');
		input.setAttribute('type', 'text');
		i1ElementsConfirm.style.display = "block"
		i2ElementsConfirm.style.display = "none"

	} else {
		target.classList.remove('view');
		input.setAttribute('type', 'password');
		i1ElementsConfirm.style.display = "block"
		i2ElementsConfirm.style.display = "block"
	}
	return false;
}


document.getElementById("btn").onclick = function submit() {
	if (firstInput.password.value == lastInput.passwordconfirm.value) {
		err.style.display = "none";
		modalwindow()
	} else {
		err.innerHTML = errText;
		err.style.display = "block"
		err.classList.add('error');
	}
}

function modalwindow() {
	form.style.visibility = "collapse";
	modalText.classList.add("modal");
	modalText.innerHTML = `<button onclick="modalclose()">+</button>` + `<p>You are welcome</p>`
}

function modalclose() {
	window.location.reload("true");
}


