let arr1 = ['hello', 'world', 23, '23', null, true, { id: "str" }, ['57', 9], Symbol("id"), undefined, 7890n, function(){}];
let type = prompt("Enter type: 'string, number, boolean, object, symbol, undefined, bigint, function'");

function filterBy(arr, type) {
  return arr.filter(e => typeof e === type);
};

console.log(filterBy(arr1, type));