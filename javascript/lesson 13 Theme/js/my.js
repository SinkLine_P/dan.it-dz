let form = document.getElementById("theme-form");
let formInput = document.getElementById("theme-input");
let textGray = document.getElementById("theme-text");
let labelP = document.getElementById("txt-label");
let link = document.getElementById("theme-link");
let linkDoc = document.getElementById("linkDoc");
let linkNav = document.getElementById("theme-link-nav");
let navbarBack = document.getElementById("theme-navbar");
let navbarLogo = document.getElementById("logo");
let navbarLink = document.getElementById("navbar-auth");
let themeOr = document.getElementById("theme-or");
let buttonTheme = document.getElementById("btn");


buttonTheme.addEventListener('click', followTheme);
document.addEventListener('DOMContentLoaded', followThemeChange);

function followTheme() {
    localStorage.setItem('mode', (localStorage.getItem('mode') || 'dark') === 'dark' ? 'light' : 'dark');
    localStorage.getItem('mode') === 'dark' ? themedark() : themelight()
}

function followThemeChange() {
    ((localStorage.getItem('mode') || 'dark') === 'dark') ? themedark() : themelight()
}

function themedark() {
    themeOr.classList.remove("or")
    textGray.classList.remove("sign");
    linkDoc.classList.remove("container-i")
    navbarLink.classList.remove("navbar-text");

    themeOr.classList.add("theme-or")
    textGray.classList.add("theme-link-and-text-S")
    linkDoc.classList.add("theme-link");
    form.classList.add("theme-form");
    labelP.classList.add("theme-link-and-text")
    link.classList.add("theme-link");
    formInput.classList.add("theme-form-input");
    formInput.classList.add("theme-form-input-placeholder");
    navbarBack.classList.add("theme-navbar-background");
    navbarLogo.classList.add("theme-logo");
    navbarLink.classList.add("theme-navbar-auth");
    buttonTheme.classList.add("dark-theme");


}

function themelight() {
    themeOr.classList.remove("theme-or")
    textGray.classList.remove("theme-link-and-text-S")
    linkDoc.classList.remove("theme-link");
    form.classList.remove("theme-form");
    labelP.classList.remove("theme-link-and-text")
    link.classList.remove("theme-link");
    formInput.classList.remove("theme-form-input");
    formInput.classList.remove("theme-form-input-placeholder");
    navbarBack.classList.remove("theme-navbar-background");
    navbarLogo.classList.remove("theme-logo");
    navbarLink.classList.remove("theme-navbar-auth");
    buttonTheme.classList.remove("dark-theme");

    themeOr.classList.add("or")
    textGray.classList.add("sign");
    linkDoc.classList.add("container-i")
    navbarLink.classList.add("navbar-text");



}


