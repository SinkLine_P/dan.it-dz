const btn = document.getElementsByClassName("btn")
window.addEventListener("keydown", keyboardPressHandler)


let obj = {
    enter: btn[0],
    s: btn[1],
    e: btn[2],
    o: btn[3],
    n: btn[4],
    l: btn[5],
    z: btn[6]
}

let blue = "#1100ff";
let black = "black";

function keyboardPressHandler(ev) {
    switch (ev.key) {
        case "Enter":
            obj.enter.style.backgroundColor = blue;
            break;
        default:
            obj.enter.style.backgroundColor = black;
            break;
    }

    switch (ev.key) {
        case "s":
            obj.s.style.backgroundColor = blue;
            break;
        case "S":
            obj.s.style.backgroundColor = blue;
            break;
        default:
            obj.s.style.backgroundColor = black;
            break;
    }

    switch (ev.key) {
        case "e":
            obj.e.style.backgroundColor = blue;
            break;
        case "E":
            obj.e.style.backgroundColor = blue;
            break;
        default:
            obj.e.style.backgroundColor = black;
            break;
    }

    switch (ev.key) {
        case "o":
            obj.o.style.backgroundColor = blue;
            break;
        case "O":
            obj.o.style.backgroundColor = blue;
            break;
        default:
            obj.o.style.backgroundColor = black;
            break;
    }

    switch (ev.key) {
        case "n":
            obj.n.style.backgroundColor = blue;
            break;
        case "N":
            obj.n.style.backgroundColor = blue;
            break;
        default:
            obj.n.style.backgroundColor = black;
            break;
    }

    switch (ev.key) {
        case "l":
            obj.l.style.backgroundColor = blue;
            break;
        case "L":
            obj.l.style.backgroundColor = blue;
            break;
        default:
            obj.l.style.backgroundColor = black;
            break;
    }

    switch (ev.key) {
        case "z":
            obj.z.style.backgroundColor = blue;
            break;
        case "Z":
            obj.z.style.backgroundColor = blue;
            break;
        default:
            obj.z.style.backgroundColor = black;
            break;
    }
}





