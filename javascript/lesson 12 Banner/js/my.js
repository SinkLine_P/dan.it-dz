let btn = document.getElementById("button");
let slides = document.getElementsByClassName("slide");
let slideIndex = 0;
let loopBanner = true;


function showSlides() {
  if (loopBanner) {
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
      console.log(slides[i])
    }

    slideIndex++;

    if (slideIndex > slides.length) {
      slideIndex = 1

    }

    slides[slideIndex - 1].style.display = "block";
    setTimeout(showSlides, 3000);

  }
}

function startLoop() {
  loopBanner = true;
  showSlides();
}

function stopLoop() {
  loopBanner = false;

}

startLoop();
